#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define F "FORWARD"
#define B "BACKWARD"
#define R "RIGHT"
#define L "LEFT"
#define S "STOP"

//#define DBG

enum MapDirection{
    NORTH = 0,
    WEST  = 1,
    EAST  = 2,
    SOUTH = 3,
};
enum MoveDirection{
    FORWARD  = 0,
    BACKWARD = 1,
};
enum MapDirection  mapDir;
enum MoveDirection moveDir;

void turnDirection(int n){
    switch(mapDir){
        case NORTH:
        if(n) mapDir = EAST;
        else  mapDir = WEST;
        break;
        case WEST:
        if(n) mapDir = NORTH;
        else  mapDir = SOUTH;
        break;
        case EAST:
        if(n) mapDir = SOUTH;
        else  mapDir = NORTH;
        break;
        case SOUTH:
        if(n) mapDir = WEST;
        else  mapDir = EAST;
        break;
        default:
        break;
    }
}

void moveMap(int step, int* i, int* j){
    switch(mapDir){
        case NORTH:
        if(moveDir) *j -= step;
        else *j += step;
        break;
        case WEST:
        if(moveDir) *i += step;
        else *i -= step;
        break;
        case EAST:
        if(moveDir) *i -= step;
        else *i += step;
        break;
        case SOUTH:
        if(moveDir) *j += step;
        else *j -= step;
        break;
        default:
        break;
    }
}

int main(int argc, const char * argv[]) {
    int col,row;
    int i,j;
    char command[10];
    for(;;){
        scanf("%d %d", &row, &col);
        if (row == 0 && col ==0) break;
        i = j = 1;
        mapDir  = NORTH;
        moveDir = FORWARD;
        for(;;){
            scanf("%s",command);
            if (strcmp(command,F) == 0){
                moveDir = FORWARD;
            }else if(strcmp(command,B) == 0){
                moveDir = BACKWARD;
            }else if(strcmp(command,R) == 0){
                turnDirection(1);
            }else if(strcmp(command,L) == 0){
                turnDirection(0);
            }else if(strcmp(command,S) == 0){
                printf("%d %d\n", i, j);
                break;
            }else{
                moveMap(atoi(command),&i,&j);
                if (i <= 0)  i = 1;
                if (row < i) i = row;
                if (j <= 0)  j = 1;
                if (col < j) j = col;
#ifdef DBG
                printf("(i,j) = (%d,%d) Dir = %d\n", i, j, mapDir);
#endif
            }

        }
    }
    return 0;
}