#include <stdio.h>

int main(){
	int n,a,b;
	int i,j,unable_cnt;
	int permutation[1000001];
	for(;;){
		scanf("%d %d %d",&n,&a,&b);
		if(n==0&&a==0&&b==0) break;
		if(n<a&&n<b) continue;
		unable_cnt=0;
		for(i=0;i<1000001;i++) permutation[i]=0;
		for(i=0;a*i<=n;i++){
			for(j=0;j<a;j++){
				if(a*i+b*j<=n) permutation[a*i+b*j]++;
			}
		}
		for(i=1;i<=n;i++){
			if(permutation[i]==0) unable_cnt++;
		}
		printf("%d\n",unable_cnt);
	}
}
